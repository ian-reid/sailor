# SailorCSS

![npm](https://img.shields.io/npm/dy/sailorcss?style=flat-square)
![npm](https://img.shields.io/npm/v/sailorcss?style=flat-square)
![npm](https://img.shields.io/npm/l/sailorcss?style=flat-square)

<p>
  <a href="https://www.sailorcss.com/" rel="nofollow">
    <img alt="Sailor CSS" src="https://gitlab.com/ian-reid/sailor/-/raw/master/logo.png" width="120" height="120">
  </a><br>
  Sailor CSS is a small and customizable layout-focused CSS library. It includes a fraction-based spacing system, CSS grid and Flexbox utility classes in addition to a few useful extras.</p><p>See it as an alternative between big CSS frameworks and a 100% custom approach.
</p>


## Documentation and Website

For documentation, visit [sailorcss.com](https://www.sailorcss.com)

## Requirements

You need SASS `^ 1.34` and we recommend using PurgeCSS for your production builds.
